function ajaxStuff() {
    $("#collapseOne").on("show.bs.collapse", function () {
        $.get("ajax1.html", (data) => $("#cardContainer1").html(data));
    });
    $("#collapseTwo").on("show.bs.collapse", function () {
        $.get("ajax2.html", (data) => $("#cardContainer2").html(data));
    });
    $("#collapseThree").on("show.bs.collapse", function () {
        $.get("ajax3.html", (data) => $("#cardContainer3").html(data));
    });
    $("#ajaxBtn1").click(function () {
        console.time("ajax1");
        console.time("ajax2");
        console.time("ajax3");
        console.time("total");
        let def1 = $.get("ajax1.html", function (data) {
            $("#container1").html(data);
            console.timeEnd("ajax1");
        });
        let def2 = $.get("ajax2.html", function (data) {
            $("#container2").html(data);
            console.timeEnd("ajax2");
        });
        let def3 = $.get("ajax3.html", function (data) {
            $("#container3").html(data);
            console.timeEnd("ajax3");
        });
        $.when(def1, def2, def3).done(function () {
                console.timeEnd("total");
            }
        )
    });
    $("#ajaxBtn2").click(function () {
        console.time("ajax1");
        console.time("ajax2");
        console.time("ajax3");
        $.get("ajax1.html", function (data) {
            $("#container1").html(data);
            console.timeEnd("ajax1");
        }).then(function () {
            $.get("ajax2.html", function (data) {
                $("#container2").html(data);
                console.timeEnd("ajax2");
            }).then(function () {
                $.get("ajax3.html", function (data) {
                    $("#container3").html(data);
                    console.timeEnd("ajax3");
                });
            });
        });
    });
    $("#ajaxBtn3").click(function () {
        let d1 = $.get("ajax1.html", (data) => $("#container1").html(data));
        let d2 = $.get("ajax2.html", (data) => $("#container2").html(data));
        let d3 = $.get("ajax3.html", (data) => $("#container3").html(data));

        function mySlide(containerSelector, duration) {
            let def = $.Deferred();
            $(containerSelector).slideUp(duration, def.resolve);
            return def;
        }

        $.when(d1, d2, d3).done(function () {
            $("#container1").show();
            $("#container2").show();
            $("#container3").show();
            let myDef1 = mySlide("#container1", 1000);
            let myDef2 = mySlide("#container2", 2000);
            let myDef3 = mySlide("#container3", 3000);

            $.when(myDef1, myDef2, myDef3).done(function () {
                alert("Мирный Deferred в каждый дом!");
            });
        });


    });
}

function animations() {
    $("#hideBtn").click(function () {
        $("#image1").hide();
    });
    $("#showBtn").click(function () {
        $("#image1").show();
    });
    $("#toogleBtn").click(function () {
        $("#toggleBtn").toggle(function () {
            $("#image1").toggle();
        });
    });

    $("#slideUpBtn").click(function () {
        $("#image1").slideUp();
    });
    $("#slideDownBtn").click(function () {
        $("#image1").slideDown();
    });
    $("#slideToggleBtnBtn").click(function () {
        $("#image1").slideToggle();
    });

    $("#sizeAnimBtn").click(function () {
        let targetWidth = parseInt($("#targetSizeInput").val());
        let animSpeed = parseInt($("#animSpeedInput").val());
        $("#image1").animate({width: targetWidth}, animSpeed);
    });

    $("#complexAnimBtn").click(function () {
        let targetWidth = parseInt($("#targetSizeInput").val());
        let animSpeed = parseInt($("#animSpeedInput").val());
        let oldWidth = $("#image1").width();
        $("#image1").animate({width: targetWidth}, animSpeed)
            .animate({width: oldWidth}, animSpeed);
    });
}

function isValidName(name) {
    return (name != null && name.length > 0);
}

function isValidCost(cost) {
    return (!isNaN(parseInt(cost)) && cost.length > 0);
}

$(document).ready(function () {

    ajaxStuff();

    animations();

    $("#newExpenseName1").focusout(function () {
        let newExpenseNameInput = $("#newExpenseName1");
        let newName = newExpenseNameInput.val();
        if (isValidName(newName)) {
            newExpenseNameInput.removeClass("is-invalid");
        } else {
            newExpenseNameInput.addClass("is-invalid");
        }
        ;
    });

    $("#newExpenseCost1").focusout(function () {
        let newCost = $("#newExpenseCost1").val();
        if (isValidCost(newCost)) {
            $("#newExpenseCost1").removeClass("is-invalid");
        } else {
            $("#newExpenseCost1").addClass("is-invalid");
        }
    });

    $(`#newExpenseAddBtn1`).click(function () {

    });
});