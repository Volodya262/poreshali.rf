let calculationsJson = `{
    "calculations": [
        {
            "calculationId":"calculation1",
            "calculationTitle":"Расчет из JSON 1",
            "calculationPeople":"Вася, Петя, Олег",
            "calculationExpences":"Вода, баня, венички"
        },
        {
            "calculationId":"calculation2",
            "calculationTitle":"Расчет из JSON 2",
            "calculationPeople":"Вася, Петя, Олег",
            "calculationExpences":"Вода, баня, венички"
        }
    ]
}`;
function getCardsAmount() { //пока нигде не используется, мб удалить?
    return cardsAmount = $(".calculationCard").length;
}


class CalculationCard {
    constructor(calculationId, calculationTitle, calculationPeople, calculationExpences) {
        this.calculationId = calculationId;
        this.calculationTitle = calculationTitle;
        this.calculationPeople = calculationPeople;
        this.calculationExpences = calculationExpences;
    }
    getHtml() {
        return `<div class='card calculationCard m-2' id='${this.calculationId}'>
            <div class='card-body'>
            <h5 class='card-title calculationTitle'>${this.calculationTitle}</h5>
            <p class='card-text calculationPeople'>
            <strong>Люди: </strong>${this.calculationPeople}</p>
            <p class='card-text calculationExpences'>
            <strong>Траты: </strong>${this.calculationExpences}</p>
            <div class='form-row justify-content-between'>
            <a href='#' class='btn btn-primary m-1'>Открыть</a>
            <a href='#' class='btn btn-danger m-1 calculationDelBtn' data-toggle="modal" data-target="#deleteModal" data-calculation-id="${this.calculationId}">Удалить</a>
            </div>
            </div >
            </div >`;
    }
}

var calculations;
function parseCalculationsTextArea() { //парсит json из textarea и кладет его в переменную calculatuions
    let jsonText = $("#jsonInputTextArea").val();
    calculations = JSON.parse(jsonText);
    calculations = calculations.calculations; //необходимое зло
}

function deleteCalculation(calculationId) {//отправляет POST, удаляет локально и удаляет карточку
    alert("Отправляем POST на сервак");
    calculations = calculations.filter(i => i.calculationId != calculationId); //удаляем локально
    $(`#${calculationId}`).remove(); //удаляем карточку
}

function drawAllCards() { //рисует все карточки из calculations (не использовать для перерисовки)
    $(".calculationCard").remove();
    calculations.forEach(i => {
        let newCard = new CalculationCard(i.calculationId, i.calculationTitle, i.calculationPeople, i.calculationExpences);
        drawCard(newCard);
    });
}

function drawCard(card) { //отрисовывает CalculationCard
    $("#cardsStart").after(card.getHtml());
}

$(document).ready(function () {
    //alert('Сработал ready');
    $("#btnParseJson").click(function () {
        parseCalculationsTextArea();
        drawAllCards();
    });

    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var calculationId = button.data('calculation-id') // Extract info from data-* attributes
        let calculationTitle = calculations.filter(i => i.calculationId == calculationId)[0].calculationTitle; //по id находим заголовок этого расчета

        var modal = $(this);
        modal.find("#modalCalculationName").text(calculationTitle);
        var modalButton = modal.find("#modalDeleteBtn");
        modalButton.data('calculation-id', calculationId); //присваиваем кнопке "удалить" в модалке в data-calculation-id calculationId
    });

    $("#modalDeleteBtn").on('click', function (event) {
        let calculationId = $(this).data('calculation-id'); // Extract info from data-* attributes
        deleteCalculation(calculationId);
    });

    $("#jsonInputTextArea").val(calculationsJson);
});

