const buyersJson = `{
    "buyers": [
        {
            "buyerId": "buyer1",
            "buyerName": "Иван",
            "expenses": [
                {
                    "name": "Подарок",
                    "cost": 5000
                },
                {
                    "name": "Чай и конфетки",
                    "cost": 800
                }
            ]
        },
        {
            "buyerId": "buyer2",
            "buyerName": "Алексей",
            "expenses": [
                {
                    "name": "Фрукты, мандаринки",
                    "cost": 800
                },
                {
                    "name": "Чай и конфетки",
                    "cost": 400
                },
                {
                    "name": "Торт",
                    "cost": 2000
                }
            ]
        },
        {
            "buyerId": "buyer3",
            "buyerName": "Олеся",
            "expenses": [
                {
                    "name": "Цветы",
                    "cost": 3000
                }
            ]
        }
    ]
}`;

class BuyerCard {
    constructor(buyerId, buyerName, expenses) {
        this.buyerId = buyerId;
        this.buyerName = buyerName;
        this.expenses = expenses;//массив структур вида {name, cost}
    }

    static getExpenseHtml(expenseName, expenseCost) {
        return `
                <div class="form-row my-2 expenseRow">
                    <div class="col-md-5 col-12">
                        <input type="text" class="form-control expenseName" placeholder="Название траты" value="${expenseName}">
                    </div>
                    <div class="col-md-4 col-7">
                        <div class="input-group">
                            <input type="text" class="form-control expenseCost" placeholder="Стоимость" value="${expenseCost}">
                            <div class="input-group-append">
                                <span class="input-group-text">₽</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-5">
                        <button class="btn btn-outline-danger btn-block expenseDelBtn">
                            Удалить
                        </button>
                    </div>
                </div>`
    }

    static getNewExpenseHtml() {
        return `<div class="form-row my-1 newExpenseRow">
                    <div class="col-md-5 col-12">
                        <input type="text" class="form-control newExpenseName" placeholder="Новая трата">
                    </div>
                    <div class="col-md-4 col-7">
                        <div class="input-group">
                            <input type="text" class="form-control newExpenseCost" placeholder="Стоимость">
                            <div class="input-group-append">
                                <span class="input-group-text">₽</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-5">
                        <button class="btn  btn-outline-primary btn-block newExpenseAddBtn">
                            Добавить
                        </button>
                    </div>
                </div>`
    }

    getBuyerNameHtml() {
        return `
                <div class="form-row align-items-center">
                    <div class="col-md-9 col-12">
                        <input type="text" class="form-control form-control-lg buyerName" placeholder="Имя" value="${this.buyerName}">
                    </div>
                    <div class="col-md-3 col-12">
                        <button class="btn btn-lg btn-danger btn-block buyerDelBtn">
                            Удалить
                        </button>
                    </div>
                </div>`
    }

    getHtml() {
        let expensesHtml = "";
        if (this.expenses != null) {
            this.expenses.forEach(expense => {
                expensesHtml += BuyerCard.getExpenseHtml(expense.name, expense.cost);
            });
        }
        return `
        <div class="card m-2 buyerCard" id=${this.buyerId}>
            <div class="card-body">
            ${this.getBuyerNameHtml()}
            ${expensesHtml}
            ${BuyerCard.getNewExpenseHtml()}
            </div>
        </div>`
    }
}

let buyersCounter = 0;

//парсит текстовую область и возвращает как структуру
function parseBuyersTextArea() {
    let textAreaText = $("#jsonInputTextArea").val();
    let buyers = JSON.parse(textAreaText).buyers;
    buyersCounter = buyers.length;
    return buyers;
}

function drawCard(card) {
    $("#cardsStart").after(card.getHtml());
}

function drawCardAtEnd(card) {
    $("#cardsEnd").before(card.getHtml());
}

function drawAllCards(buyers) {
    $(".buyerCard").remove();
    buyers.forEach(i => {
        drawCardAtEnd(new BuyerCard(i.buyerId, i.buyerName, i.expenses));
    });
}

//временный костыль для получения нового id. Откуда потом брать id?
function getNewBuyerId() {
    buyersCounter++;
    return "buyer" + buyersCounter;
}

//создает нового покупателя
function createNewBuyer(name) {
    let buyerId = getNewBuyerId();
    let newBuyerCard = new BuyerCard(buyerId, name, undefined);
    drawCardAtEnd(newBuyerCard);
    assignCardBtnHandlers();
}

//обработчик кнопки удаления траты. Вынесен в отдельную функцию, т.к. используется более одного раза
function expenseDelBtnHandler(event) {
    let button = event.currentTarget;
    let row = $(button).parent().parent(); //form-row с названием траты, стоимостью и самой кнопкой
    row.remove();
}

//очищает и затем присваивает всем кнопкам карточек их хендлеры
function assignCardBtnHandlers() {
    $(".expenseDelBtn").off("click").click(expenseDelBtnHandler);

    //TODO: сделать это через модалку
    $(".buyerDelBtn").off("click").click(function (event) {
        let button = event.currentTarget;
        $(button).parents().filter(".card").remove();
    });

    $(".newExpenseAddBtn").off("click").click(function (event) {
        let button = event.currentTarget;
        let row = $(button).parent().parent();
        let expenseName = row.find(".newExpenseName").val();
        let expenseCost = row.find(".newExpenseCost").val();

        if (expenseName.length > 0 && expenseCost.length > 0)
            row.before(BuyerCard.getExpenseHtml(expenseName, expenseCost));
        else {
            alert("Сделай норм валидацию!")
        }
        assignCardBtnHandlers();
        row.find(".newExpenseName").val("");
        row.find(".newExpenseCost").val("");
    })
}

function serializeCards() {
    let buyers = {buyers: []};
    let cards = $(".buyerCard").toArray();
    cards.forEach(function (card) {
        let buyerId = $(card).attr("id");
        let buyerName = $(card).find(".buyerName").val();
        let expenseRows = $(card).find(".expenseRow").toArray();
        let expenses = [];
        expenseRows.forEach(function (row) {
            let name = $(row).find(".expenseName").val();
            let cost = $(row).find(".expenseCost").val();
            expenses.push({name: name, cost: cost});
        });
        buyers.buyers.push({buyerId: buyerId, buyerName: buyerName, expenses: expenses});
    });
    return JSON.stringify(buyers);
};

$(document).ready(function () {
    $("#jsonInputTextArea").val(buyersJson);

    $("#newBuyerBtn").click(function () {
        let name = $("#newBuyerName").val(); //достаем имя
        if (name.length > 0)
            createNewBuyer(name); //создаем новую карточку
        else alert("Сделай норм валидацию!");

        $("#newBuyerName").val(""); //очищаем поле ввода
        $(".buyerCard:last").find(".newExpenseName").focus();
    });

    $("#btnParseJson").click(function () {
        let buyers = parseBuyersTextArea();
        drawAllCards(buyers);
        assignCardBtnHandlers();
    });

    $("#btnFormJson").click(function () {
        let buyers = serializeCards(); //json
        $("#jsonOutputTextArea").val(buyers);
    })

});