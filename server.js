var http = require('http');
var static = require('node-static');
var file = new static.Server('.');

if (static==undefined)
  console.log('static is undefined');
if (static==null)
  console.log('static is null')
http.createServer(function(req, res) {
  file.serve(req, res);
}).listen(8080);

console.log('Server running on port 8080');